package com.caesar84mx.hallofheroes.mainScreen.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.hallofheroes.R
import com.caesar84mx.hallofheroes.commons.kotlinextensions.onClick
import com.caesar84mx.hallofheroes.mainScreen.view.OnItemClickListener
import com.caesar84mx.library.main_screen.data.model.HeroListItemUI
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_heroes_item.view.*

class HeroUIViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
    fun bind(viewmodel: HeroListItemUI, onItemClickListener: OnItemClickListener) {
        view.tvName.text = viewmodel.name
        view.tvTeam.text = viewmodel.team
        view.tvFirstAppearance.text = viewmodel.firstAppearance.toString()
        Picasso.get()
            .load(viewmodel.avatarUrl)
            .placeholder(R.drawable.logo_empty)
            .error(R.drawable.logo_empty)
            .into(view.ivAvatar)

        view.onClick {
            onItemClickListener(viewmodel.id)
        }
    }
}