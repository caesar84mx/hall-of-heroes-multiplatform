package com.caesar84mx.hallofheroes.mainScreen.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.caesar84mx.hallofheroes.R
import com.caesar84mx.library.common.kotlinextensions.notNull
import com.caesar84mx.library.main_screen.data.model.HeroListItemUI
import com.caesar84mx.library.main_screen.viewmodel.MainScreenCommonViewModel
import com.caesar84mx.library.main_screen.viewmodel.MainScreenViewModel
import kotlinx.android.synthetic.main.fragment_main_view.*

class MainScreenFragment: Fragment() {
    private val viewmodel: MainScreenViewModel by lazy { MainScreenCommonViewModel() }
    private val adapter = HeroesAdapter()
    private var isScreenShown = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_main_view, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvHeroesList.layoutManager = LinearLayoutManager(context)
        rvHeroesList.adapter = adapter.apply {
            onItemClickListener = { id ->
                openDetails(id.notNull().toLong())
            }
        }

        viewmodel.heroesLiveData.addObserver {
            if (isScreenShown) {
                onStatusChanged(it)
            }
        }

        viewmodel.checkHeroes()
        viewmodel.observeHeroes { heroes ->
            if(isScreenShown) {
                onSuccess(heroes)
            }
        }
    }

    override fun onStart() {
        isScreenShown = true
        super.onStart()
    }

    private fun openDetails(id: Long) {
        findNavController().navigate(
            MainScreenFragmentDirections.actionMainScreenFragmentToHeroDetailsFragment(id)
        )
    }
    
    private fun onStatusChanged(status: MainScreenViewModel.Status) {
        when(status) {
            is MainScreenViewModel.Status.Nothing -> { /* Do nothing */ }

            is MainScreenViewModel.Status.Loading -> onLoading()

            is MainScreenViewModel.Status.Success -> onSuccess(status.payload)

            is MainScreenViewModel.Status.Error -> onError(status.msg)
        }
    }

    private fun onSuccess(payload: List<HeroListItemUI>) {
        pbProgressBar.visibility = View.GONE

        if (payload.isEmpty()) {
            rvHeroesList.visibility = View.GONE
            tvNotFound.visibility = View.VISIBLE
        } else {
            rvHeroesList.visibility = View.VISIBLE
            tvNotFound.visibility = View.GONE

            adapter.setData(payload)
        }
    }

    private fun onLoading() {
        pbProgressBar.visibility = View.VISIBLE
        rvHeroesList.visibility = View.GONE
        tvNotFound.visibility = View.GONE
    }

    private fun onError(msg: String) {
        pbProgressBar.visibility = View.GONE
        rvHeroesList.visibility = View.GONE
        tvNotFound.visibility = View.VISIBLE

        AlertDialog.Builder(context!!).setTitle("Error").setMessage(msg).show()
    }

    override fun onPause() {
        viewmodel.stopObserving()
        isScreenShown = false
        super.onPause()
    }

    override fun onDestroy() {
        viewmodel.heroesLiveData.removeObserver {  }
        viewmodel.onCleared()
        super.onDestroy()
    }
}