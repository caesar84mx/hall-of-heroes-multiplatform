package com.caesar84mx.hallofheroes.mainScreen.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.caesar84mx.hallofheroes.R
import com.caesar84mx.hallofheroes.mainScreen.viewHolders.HeroUIViewHolder
import com.caesar84mx.library.main_screen.data.model.HeroListItemUI

class HeroesAdapter : RecyclerView.Adapter<HeroUIViewHolder>() {
    private val data: MutableList<HeroListItemUI> = mutableListOf()
    var onItemClickListener: OnItemClickListener = {  }

    fun setData(newData: List<HeroListItemUI>) {
        data.clear()
        data.addAll(newData)
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroUIViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_heroes_item, parent, false)
        return HeroUIViewHolder(itemView)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: HeroUIViewHolder, position: Int) {
        holder.bind(data[position], onItemClickListener)
    }
}

typealias OnItemClickListener = (Long?) -> Unit