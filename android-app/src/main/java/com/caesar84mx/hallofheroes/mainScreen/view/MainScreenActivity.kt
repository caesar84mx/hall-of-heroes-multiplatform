package com.caesar84mx.hallofheroes.mainScreen.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.caesar84mx.hallofheroes.R
import com.caesar84mx.library.Database
import com.caesar84mx.library.common.data.repositories.CommonDatabase
import com.squareup.sqldelight.android.AndroidSqliteDriver

class MainScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val driver = AndroidSqliteDriver(Database.Schema, this.applicationContext, "main.db")
        CommonDatabase.driver = driver
    }
}
