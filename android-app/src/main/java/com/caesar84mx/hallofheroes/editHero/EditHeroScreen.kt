package com.caesar84mx.hallofheroes.editHero

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.caesar84mx.hallofheroes.BR
import com.caesar84mx.hallofheroes.R
import com.caesar84mx.hallofheroes.commons.kotlinextensions.bakeToast
import com.caesar84mx.hallofheroes.commons.kotlinextensions.onClick
import com.caesar84mx.hallofheroes.databinding.FragmentEditHeroBinding
import com.caesar84mx.library.edit_screen.viewmodel.EditHeroCommonViewModel
import com.caesar84mx.library.edit_screen.viewmodel.EditHeroViewModel
import dev.icerock.moko.mvvm.MvvmFragment
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.android.synthetic.main.fragment_edit_hero.*

class EditHeroScreenFragment: MvvmFragment<FragmentEditHeroBinding, EditHeroCommonViewModel>() {
    override val layoutId: Int = R.layout.fragment_edit_hero
    override val viewModelClass: Class<EditHeroCommonViewModel> = EditHeroCommonViewModel::class.java
    override val viewModelVariableId: Int = BR.vm

    override fun viewModelFactory(): ViewModelProvider.Factory = createViewModelFactory {
        val id = EditHeroScreenFragmentArgs.fromBundle(arguments ?: Bundle()).id
        EditHeroCommonViewModel(id)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.status.addObserver(::onStatusChanged)
        btnCancel.onClick { activity?.onBackPressed() }
    }

    private fun onStatusChanged(status: EditHeroViewModel.Status) {
        when(status) {
            is EditHeroViewModel.Status.Idle -> { /* Do nothing **/ }
            is EditHeroViewModel.Status.SavingSuccess -> {
                activity?.bakeToast("Saved successfully")
                activity?.onBackPressed()
            }
            is EditHeroViewModel.Status.SavingError -> {
                activity?.bakeToast("Error saving hero!")
            }
        }
    }

    override fun onDestroy() {
        viewModel.onCleared()
        super.onDestroy()
    }
}