package com.caesar84mx.hallofheroes.commons.kotlinextensions

import android.app.Activity
import android.widget.Toast


fun Activity.bakeToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}