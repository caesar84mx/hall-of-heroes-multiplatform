package com.caesar84mx.hallofheroes.commons.kotlinextensions

import android.view.View

fun View.onClick(block: (View) -> Unit) {
    setOnClickListener { v -> block(v) }
}