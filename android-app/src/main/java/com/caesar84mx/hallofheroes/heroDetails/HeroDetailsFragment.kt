package com.caesar84mx.hallofheroes.heroDetails

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.caesar84mx.hallofheroes.R
import com.caesar84mx.hallofheroes.BR
import com.caesar84mx.hallofheroes.commons.kotlinextensions.onClick
import com.caesar84mx.hallofheroes.databinding.FragmentHeroDetailsBinding
import com.caesar84mx.library.hero_details_screen.viewmodel.HeroDetailsCommonViewModel
import com.squareup.picasso.Picasso
import dev.icerock.moko.mvvm.MvvmFragment
import dev.icerock.moko.mvvm.createViewModelFactory
import kotlinx.android.synthetic.main.fragment_hero_details.*

class HeroDetailsFragment : MvvmFragment<FragmentHeroDetailsBinding, HeroDetailsCommonViewModel>() {
    override val layoutId: Int = R.layout.fragment_hero_details
    override val viewModelClass: Class<HeroDetailsCommonViewModel> = HeroDetailsCommonViewModel::class.java
    override val viewModelVariableId: Int = BR.viewmodel

    private var id: Long = 0L
    private var isScreenShown = false

    override fun viewModelFactory(): ViewModelProvider.Factory = createViewModelFactory {
        id = HeroDetailsFragmentArgs.fromBundle(arguments ?: Bundle()).id
        HeroDetailsCommonViewModel(
            id
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnCancel.onClick { activity?.onBackPressed() }
        btnEdit.onClick { openEdit() }
    }

    private fun openEdit() {
        findNavController().navigate(
            HeroDetailsFragmentDirections.actionHeroDetailsFragmentToEditHeroScreenFragment2(id)
        )
    }

    override fun onStart() {
        isScreenShown = true
        viewModel.avatarUrl.addObserver {
            if (isScreenShown) {
                if (it.isNotEmpty()) {
                    Picasso.get()
                        .load(it)
                        .error(R.drawable.logo_empty)
                        .placeholder(R.drawable.logo_empty)
                        .into(ivAvatar)
                }
            }
        }
        super.onStart()
    }

    override fun onPause() {
        isScreenShown = false
        viewModel.stopObserving()
        super.onPause()
    }

    override fun onDestroy() {
        viewModel.avatarUrl.removeObserver {  }
        viewModel.onCleared()
        super.onDestroy()
    }
}