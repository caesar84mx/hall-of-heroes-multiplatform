//
//  HeroViewHolder.swift
//  ios-app
//
//  Created by Maxim Dymnov on 2/20/20.
//  Copyright © 2020 Maxim Dymnov. All rights reserved.
//

import UIKit
import MultiPlatformLibrary
import moa

class HeroViewHolder: UITableViewCell {
    
    @IBOutlet weak var tvName: UILabel!
    @IBOutlet weak var tvTeam: UILabel!
    @IBOutlet weak var tvYearOfAppearance: UILabel!
    @IBOutlet weak var ivAvatar: UIImageView!
    
    func bind(viewModel: HeroListItemUI) {
        tvName.text = viewModel.name
        tvTeam.text = viewModel.team
        tvYearOfAppearance.text = String(viewModel.firstAppearance)
        ivAvatar.moa.url = viewModel.avatarUrl
    }
}
