//
//  ViewController.swift
//  Hall of Heroes
//
//  Created by Maxim Dymnov on 2/13/20.
//  Copyright © 2020 Maxim Dymnov. All rights reserved.
//

import UIKit
import MultiPlatformLibrary

class MainScreenViewController: UIViewController {

    @IBOutlet var heroesTable: UITableView!
    
    var currentList = [HeroListItemUI]()
    let viewmodel = MainScreenCommonViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        heroesTable.tableFooterView = UIView()
        
        viewmodel.heroesLiveData.addObserver(observer: { (status) in
            self.onStatusChanged(status: status!)
        })
        viewmodel.checkHeroes()
        viewmodel.observeHeroes(onChange: { (data) in
            self.showList(heroes: data)
        })
    }
    
    private func onStatusChanged(status: MainScreenViewModel.Status) {
        switch status {
        case is MainScreenViewModel.StatusLoading: showLoading()
            
        case is MainScreenViewModel.StatusSuccess: do {
            let successStatus = status as! MainScreenViewModel.StatusSuccess
            showList(heroes: successStatus.payload)
            }
            
        case is MainScreenViewModel.StatusError: do {
            let errorStatus = status as! MainScreenViewModel.StatusError
            showError(msg: errorStatus.msg)
            }
        
        case is MainScreenViewModel.StatusNothing: do {}
        
        default: print("Unrecognized status")
        }
    }
    
    private func showLoading() {
        showSpinner()
    }
    
    private func showList(heroes: [HeroListItemUI]) {
        currentList = heroes
        heroesTable.reloadData()
        hideSpinner()
    }
    
    private func showError(msg: String) {
        hideSpinner()
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil) )
        present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetails" {
            if let indexPath = sender as? IndexPath {
                let destination = segue.destination as! DetailsViewController
                let item = currentList[indexPath.row]
                destination.heroId = item.id
            }
        }
    }
    
    deinit {
        viewmodel.onCleared()
    }
}

extension MainScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeroCell") as! HeroViewHolder
        
        return cell.ivAvatar.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(currentList.count)
        return currentList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ShowDetails", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = currentList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeroCell") as! HeroViewHolder
        cell.bind(viewModel: item)
        
        return cell
    }
}

