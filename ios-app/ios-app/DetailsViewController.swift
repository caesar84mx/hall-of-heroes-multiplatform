//
//  DetailsViewController.swift
//  ios-app
//
//  Created by Maxim Dymnov on 3/5/20.
//  Copyright © 2020 Maxim Dymnov. All rights reserved.
//

import UIKit
import MultiPlatformLibrary
import MultiPlatformLibraryMvvm
import moa

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var tvName: UILabel!
    @IBOutlet weak var tvRealName: UILabel!
    @IBOutlet weak var tvTeam: UILabel!
    @IBOutlet weak var tvCreatedBy: UILabel!
    @IBOutlet weak var tvFirstAppearance: UILabel!
    @IBOutlet weak var tvBio: UITextView!
    
    
    
    private var viewModel: HeroDetailsViewModel? = nil
    var heroId: Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = HeroDetailsCommonViewModel(id: heroId)
        bindValues()
    }
    
    private func bindValues() {
        if let vm = viewModel {
            vm.avatarUrl.addObserver(observer: { (url) in
                self.ivAvatar.moa.url = url as String?
            })
            
            tvName.bindText(liveData: vm.name)
            tvRealName.bindText(liveData: vm.realName)
            tvTeam.bindText(liveData: vm.team)
            tvCreatedBy.bindText(liveData: vm.createdBy)
            tvFirstAppearance.bindText(liveData: vm.firstAppearance)
            tvBio.bindText(liveData: vm.bio)
        }
    }
}
