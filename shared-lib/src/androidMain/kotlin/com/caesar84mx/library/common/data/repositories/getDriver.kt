package com.caesar84mx.library.common.data.repositories

import com.squareup.sqldelight.db.SqlDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

internal actual fun getDriver(): SqlDriver? = null
internal actual val mainDispatcher: CoroutineDispatcher = Dispatchers.Main
internal actual val backgroundDispatcher: CoroutineDispatcher = Dispatchers.IO