package com.caesar84mx.library.main_screen.usecase

import com.caesar84mx.library.common.data.model.Hero
import com.caesar84mx.library.common.data.model.HeroEntity
import com.caesar84mx.library.common.data.repositories.HeroesRepo
import com.caesar84mx.library.common.kotlinextensions.map
import com.caesar84mx.library.main_screen.data.networking.HeroesApi

internal class CommonHeroesUseCase : HeroesUseCase() {
    private val api: HeroesApi by lazy { HeroesApi() }
    private val repo: HeroesRepo by lazy { HeroesRepo() }

    override suspend fun checkHeroes(
        onError: (Status.Error) -> Unit,
        onSuccess: (Status.Success) -> Unit
    ) {
        val cached = repo.findAll()
        if (cached.isEmpty()) {
            api.retrieveHeroes({
                onError(Status.Error(it))
            }) { responseList ->
                responseList
                    .map<Hero, HeroEntity>()
                    .map { repo.save(it) }
            }
        } else {
            val heroes = repo.findAll()
            onSuccess(Status.Success(heroes))
        }
    }
}