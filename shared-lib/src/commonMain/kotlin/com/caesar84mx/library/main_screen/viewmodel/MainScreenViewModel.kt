package com.caesar84mx.library.main_screen.viewmodel

import com.caesar84mx.library.main_screen.data.model.HeroListItemUI
import dev.icerock.moko.mvvm.livedata.MutableLiveData
import dev.icerock.moko.mvvm.viewmodel.ViewModel

abstract class MainScreenViewModel: ViewModel() {
    val heroesLiveData: MutableLiveData<Status> = MutableLiveData(Status.Nothing)

    abstract fun onItemDelete(id: Long)

    abstract fun checkHeroes()

    abstract fun observeHeroes(onChange: (List<HeroListItemUI>) -> Unit)

    abstract fun stopObserving()

    sealed class Status {
        object Nothing : Status()
        object Loading : Status()
        data class Error(val msg: String): Status()
        data class Success(val payload: List<HeroListItemUI>): Status()
    }
}