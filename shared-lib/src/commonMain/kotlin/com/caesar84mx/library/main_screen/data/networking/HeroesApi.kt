package com.caesar84mx.library.main_screen.data.networking

import com.caesar84mx.library.common.data.model.Hero
import com.caesar84mx.library.common.kotlinextensions.launchInBackground
import io.ktor.client.HttpClient
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readText
import kotlinx.serialization.json.Json
import kotlinx.serialization.list

internal class HeroesApi {
    private val client = HttpClient()

    suspend fun retrieveHeroes(
        onError: (Throwable) -> Unit,
        onSuccess: (List<Hero>) -> Unit
    ) {
        val heroesUrl = "https://www.simplifiedcoding.net/demos/marvel/"
        try {
            val result = client.request<HttpResponse> { url(heroesUrl) }.readText()
            val heroesResponse = Json.nonstrict.parse(Hero.serializer().list, result)

            onSuccess(heroesResponse)
        } catch (ex: Throwable) {
            onError(ex)
        }
    }
}