package com.caesar84mx.library.main_screen.viewmodel

import com.caesar84mx.library.common.data.repositories.HeroesRepo
import com.caesar84mx.library.common.data.repositories.idCounter
import com.caesar84mx.library.common.kotlinextensions.map
import com.caesar84mx.library.main_screen.data.model.HeroListItemUI
import com.caesar84mx.library.main_screen.usecase.CommonHeroesUseCase
import com.caesar84mx.library.main_screen.usecase.HeroesUseCase
import kotlinx.coroutines.launch

class MainScreenCommonViewModel : MainScreenViewModel() {
    private val useCase: HeroesUseCase = CommonHeroesUseCase()
    private val repo = HeroesRepo()
    private var subscriptionId = idCounter.incrementAndGet()

    override fun onItemDelete(id: Long) {
        repo.delete(id)
    }

    override fun checkHeroes() {
        heroesLiveData.postValue(Status.Loading)
        viewModelScope.launch {
            useCase.checkHeroes({ result ->
                val errorMessage = result.throwable.message
                val msg = "An error occurred while loading data. Reason: $errorMessage"
                heroesLiveData.postValue(Status.Error(msg))
            }) {
                heroesLiveData.postValue(Status.Success(it.payload.map()))
            }
        }
    }

    override fun observeHeroes(onChange: (List<HeroListItemUI>) -> Unit) {
        repo.observe(subscriptionId) { data ->
            onChange(data.map())
        }
    }

    override fun stopObserving() {
        repo.stopObserving(subscriptionId)
        subscriptionId = idCounter.incrementAndGet()
    }

    override fun onCleared() {
        stopObserving()
        super.onCleared()
    }
}