package com.caesar84mx.library.main_screen.usecase

import com.caesar84mx.library.common.data.model.HeroEntity
import dev.icerock.moko.mvvm.livedata.MutableLiveData
import kotlin.native.concurrent.ThreadLocal

internal abstract class HeroesUseCase {
    abstract suspend fun checkHeroes(
        onError: (Status.Error) -> Unit,
        onSuccess: (Status.Success) -> Unit
    )

    sealed class Status {
        data class Error(val throwable: Throwable) : Status()
        data class Success(val payload: List<HeroEntity>) : Status()
    }
}