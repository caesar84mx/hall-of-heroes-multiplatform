package com.caesar84mx.library.main_screen.data.model

data class HeroListItemUI (
    val id: Long,
    val name: String,
    val team: String,
    val firstAppearance: Int,
    val avatarUrl: String
)