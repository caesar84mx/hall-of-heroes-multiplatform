package com.caesar84mx.library.hero_details_screen.viewmodel

import com.caesar84mx.library.common.data.model.HeroEntity
import com.caesar84mx.library.common.data.repositories.HeroesRepo
import com.caesar84mx.library.common.data.repositories.idCounter
import com.caesar84mx.library.common.kotlinextensions.notNull
import dev.icerock.moko.mvvm.livedata.LiveData
import dev.icerock.moko.mvvm.livedata.MutableLiveData
import dev.icerock.moko.mvvm.livedata.map

class HeroDetailsCommonViewModel(val id: Long) : HeroDetailsViewModel() {
    private val repo = HeroesRepo()
    private val _hero: MutableLiveData<HeroEntity?> = MutableLiveData(repo.get(id))

    private var subscriptionId = idCounter.incrementAndGet()

    override val name: LiveData<String> = _hero.map { it?.name.notNull() }
    override val realName: LiveData<String> = _hero.map { it?.realName.notNull() }
    override val team: LiveData<String> = _hero.map { it?.team.notNull() }
    override val createdBy: LiveData<String> = _hero.map { it?.createdBy.notNull() }
    override val firstAppearance: LiveData<String> =
        _hero.map { it?.firstAppearance?.toString().notNull() }
    override val bio: LiveData<String> = _hero.map { it?.bio.notNull() }
    override val avatarUrl: LiveData<String> = _hero.map { it?.avatarUrl.notNull() }

    override fun stopObserving() {
        repo.stopObserving(subscriptionId)
        subscriptionId = idCounter.incrementAndGet()
    }

    override fun onCleared() {
        _hero.value = HeroEntity()
        stopObserving()
        super.onCleared()
    }
}