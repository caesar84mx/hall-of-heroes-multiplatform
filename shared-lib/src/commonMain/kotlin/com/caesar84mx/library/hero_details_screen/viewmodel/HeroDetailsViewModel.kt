package com.caesar84mx.library.hero_details_screen.viewmodel

import dev.icerock.moko.mvvm.livedata.LiveData
import dev.icerock.moko.mvvm.viewmodel.ViewModel

abstract class HeroDetailsViewModel: ViewModel() {
    abstract val name: LiveData<String>
    abstract val realName: LiveData<String>
    abstract val team: LiveData<String>
    abstract val createdBy: LiveData<String>
    abstract val firstAppearance: LiveData<String>
    abstract val bio: LiveData<String>
    abstract val avatarUrl: LiveData<String>

    abstract fun stopObserving()
}