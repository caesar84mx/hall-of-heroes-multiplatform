package com.caesar84mx.library.common.kotlinextensions

inline fun <reified F, reified T> List<F>.map(): List<T> = map{ it.map<F, T>() }

fun <V> List<V>?.notNull(): List<V> = this ?: listOf()