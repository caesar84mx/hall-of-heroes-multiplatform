package com.caesar84mx.library.common.mappers

import com.caesar84mx.library.common.data.model.Hero
import com.caesar84mx.library.common.data.model.HeroEntity
import com.caesar84mx.library.main_screen.data.model.HeroListItemUI

class MapperFactory {
    companion object {
        var dictionary = getDictionaryMappers()

        inline fun <reified T, reified V> map(objectToMap: T): V {
            return getMapper<T, V>().map(objectToMap)
        }

        inline fun <reified T, reified V> getMapper(): Mapper<T, V> {
            require(dictionary.containsKey(Pair(T::class, V::class))) {
                "Mapper from ${T::class} to ${V::class} not found!"
            }

            return dictionary[Pair(T::class, V::class)].let { mapper -> mapper as Mapper<T, V> }
        }

        private fun getDictionaryMappers(): Map<Pair<*, *>, Mapper<*, *>> {
            val dictionary = HashMap<Pair<*, *>, Mapper<*, *>>()
            dictionary[Pair(HeroEntity::class, HeroListItemUI::class)] = HeroEntityToHeroListItemUiMapper()
            dictionary[Pair(Hero::class, HeroEntity::class)] = HeroToHeroEntityMapper()

            return dictionary
        }
    }
}