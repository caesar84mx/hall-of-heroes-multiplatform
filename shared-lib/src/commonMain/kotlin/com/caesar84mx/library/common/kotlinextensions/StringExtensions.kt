package com.caesar84mx.library.common.kotlinextensions

fun String?.notNull(): String = this ?: ""