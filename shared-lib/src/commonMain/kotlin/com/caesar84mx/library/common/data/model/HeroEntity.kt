package com.caesar84mx.library.common.data.model

data class HeroEntity(
    var id: Long? = null,
    var name: String,
    var realName: String,
    var team: String,
    var firstAppearance: Int,
    var createdBy: String,
    var publisher: String,
    var avatarUrl: String? = null,
    var bio: String
) {
    constructor(): this(
        name = "",
        realName = "",
        team = "",
        firstAppearance = 0,
        createdBy = "",
        publisher = "",
        bio = ""
    )
}