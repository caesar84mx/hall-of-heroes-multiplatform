package com.caesar84mx.library.common.mappers

import com.caesar84mx.library.common.data.model.HeroEntity
import com.caesar84mx.library.common.kotlinextensions.notNull
import com.caesar84mx.library.main_screen.data.model.HeroListItemUI

class HeroEntityToHeroListItemUiMapper: Mapper<HeroEntity, HeroListItemUI> {
    override fun map(from: HeroEntity) = with(from) {
        HeroListItemUI(
            id = id.notNull().toLong(),
            name = name,
            team = team,
            firstAppearance = firstAppearance,
            avatarUrl = avatarUrl.notNull()
        )
    }
}