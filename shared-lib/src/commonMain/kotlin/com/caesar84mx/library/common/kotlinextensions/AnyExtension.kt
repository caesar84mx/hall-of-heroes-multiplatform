package com.caesar84mx.library.common.kotlinextensions

import com.caesar84mx.library.common.mappers.MapperFactory

inline fun <reified F, reified T> F.map(): T = MapperFactory.map(this)