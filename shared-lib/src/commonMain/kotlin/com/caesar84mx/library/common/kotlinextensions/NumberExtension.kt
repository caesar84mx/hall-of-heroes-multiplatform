package com.caesar84mx.library.common.kotlinextensions

fun Number?.notNull(): Number = this ?: 0