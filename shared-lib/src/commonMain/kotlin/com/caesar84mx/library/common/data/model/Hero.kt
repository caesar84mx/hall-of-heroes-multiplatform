package com.caesar84mx.library.common.data.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Hero (
    val name: String,

    @SerialName("realname")
    val realName: String,

    val team: String,

    @SerialName("firstappearance")
    val firstAppearance: Int,

    @SerialName("createdby")
    val createdBy: String,

    val publisher: String,

    @SerialName("imageurl")
    val imageUrl: String,

    val bio: String
)