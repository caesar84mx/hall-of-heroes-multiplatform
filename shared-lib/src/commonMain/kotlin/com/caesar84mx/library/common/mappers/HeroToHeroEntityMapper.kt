package com.caesar84mx.library.common.mappers

import com.caesar84mx.library.common.data.model.Hero
import com.caesar84mx.library.common.data.model.HeroEntity

class HeroToHeroEntityMapper: Mapper<Hero, HeroEntity> {
    override fun map(from: Hero): HeroEntity = with(from) {
        HeroEntity(
            null,
            name = name,
            realName = realName, team = team,
            firstAppearance = firstAppearance,
            createdBy = createdBy,
            publisher = publisher,
            avatarUrl = imageUrl,
            bio = bio
        )
    }
}