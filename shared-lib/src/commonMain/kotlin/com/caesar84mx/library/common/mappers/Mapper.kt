package com.caesar84mx.library.common.mappers

interface Mapper<F, T> {
    fun map(from: F): T
}