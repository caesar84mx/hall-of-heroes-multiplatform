package com.caesar84mx.library.common.data.repositories

import com.caesar84mx.library.common.data.exceptions.NotExistsException
import com.caesar84mx.library.common.data.model.HeroEntity
import com.caesar84mx.library.common.kotlinextensions.launchInBackground
import com.caesar84mx.library.common.kotlinextensions.notNull

internal class HeroesRepo {
    fun save(heroEntity: HeroEntity) {
        CommonDatabase.database
            .databaseQueries
            .save(
                name = heroEntity.name,
                realName = heroEntity.realName,
                team = heroEntity.team,
                firstAppearance = heroEntity.firstAppearance.toLong(),
                createdBy = heroEntity.createdBy,
                publisher = heroEntity.publisher,
                avatarUrl = heroEntity.avatarUrl,
                bio = heroEntity.bio
            )
    }

    fun update(heroEntity: HeroEntity) {
        if (heroEntity.id == null || heroEntity.id.notNull() == 0) {
            throw NotExistsException("Entity $heroEntity does not exist!")
        }

        CommonDatabase.database.databaseQueries.update(
            id = heroEntity.id.notNull().toLong(),
            name = heroEntity.name,
            realName = heroEntity.realName,
            team = heroEntity.team,
            firstAppearance = heroEntity.firstAppearance.toLong(),
            createdBy = heroEntity.createdBy,
            publisher = heroEntity.publisher,
            avatarUrl = heroEntity.avatarUrl,
            bio = heroEntity.bio
        )
    }

    fun get(id: Long): HeroEntity? = CommonDatabase.database
        .databaseQueries
        .get(id) { entityId: Long, name: String, realName: String, team: String, firstAppearance: Long, createdBy: String, publisher: String, avatarUrl: String?, bio: String ->
            HeroEntity(
                id = entityId,
                name = name,
                realName = realName,
                team = team,
                firstAppearance = firstAppearance.toInt(),
                createdBy = createdBy,
                publisher = publisher,
                avatarUrl = avatarUrl,
                bio = bio
            )
        }
        .executeAsOneOrNull()

    fun liveGet(observerId: Int, id: Long, onChange: (HeroEntity?) -> Unit) {
        CommonDatabase.observeSingle(observerId, id, onChange)
    }

    fun delete(id: Long) {
        launchInBackground {
            CommonDatabase.database.databaseQueries.delete(id)
        }
    }

    fun findAll(): List<HeroEntity> = CommonDatabase.findAll()

    fun clearAll() {
        CommonDatabase.database.databaseQueries.clear()
    }

    fun observe(id: Int, onChange: (List<HeroEntity>) -> Unit) {
        CommonDatabase.observe(id, onChange)
    }

    fun stopObserving(id: Int) {
        CommonDatabase.stopObserving(id)
    }
}