package com.caesar84mx.library.common.data.repositories

import com.caesar84mx.library.Database
import com.caesar84mx.library.common.data.model.HeroEntity
import com.squareup.sqldelight.Query
import com.squareup.sqldelight.db.SqlDriver
import kotlinx.atomicfu.atomic
import kotlinx.coroutines.CoroutineDispatcher
import kotlin.native.concurrent.ThreadLocal

internal expect fun getDriver(): SqlDriver?

@ThreadLocal
internal expect val mainDispatcher: CoroutineDispatcher

@ThreadLocal
internal expect val backgroundDispatcher: CoroutineDispatcher

@ThreadLocal
object CommonDatabase {
    var driver: SqlDriver? = getDriver()
    val database: Database by lazy { Database(driver!!) }

    private val observers: MutableMap<Int, Query.Listener> = mutableMapOf()

    fun observe(id: Int, callback: (List<HeroEntity>) -> Unit) {
        if (id in observers.keys) {
            throw RuntimeException("Already observing id $id")
        } else {
            val listener = object : Query.Listener {
                override fun queryResultsChanged() {
                    callback(
                        database
                            .databaseQueries
                            .findAll { entityId: Long, name: String, realName: String, team: String, firstAppearance: Long, createdBy: String, publisher: String, avatarUrl: String?, bio: String ->
                                HeroEntity(
                                    id = entityId,
                                    name = name,
                                    realName = realName,
                                    team = team,
                                    firstAppearance = firstAppearance.toInt(),
                                    createdBy = createdBy,
                                    publisher = publisher,
                                    avatarUrl = avatarUrl,
                                    bio = bio
                                )
                            }.executeAsList()
                    )
                }
            }

            observers[id] = listener
            database.databaseQueries.findAll().addListener(listener)
        }
    }

    fun observeSingle(id: Int, entityId: Long, onChange: (HeroEntity?) -> Unit) {
        if (id in observers.keys) {
            throw RuntimeException("Already observing id $id")
        } else {
            val listener = object : Query.Listener {
                override fun queryResultsChanged() {
                    onChange(
                        database.databaseQueries.get(entityId) { entityId: Long, name: String, realName: String, team: String, firstAppearance: Long, createdBy: String, publisher: String, avatarUrl: String?, bio: String ->
                            HeroEntity(
                                id = entityId,
                                name = name,
                                realName = realName,
                                team = team,
                                firstAppearance = firstAppearance.toInt(),
                                createdBy = createdBy,
                                publisher = publisher,
                                avatarUrl = avatarUrl,
                                bio = bio
                            )
                        }.executeAsOneOrNull()
                    )
                }
            }

            observers[id] = listener
            database.databaseQueries.get(entityId).addListener(listener)
        }
    }

    fun stopObserving(id: Int) {
        observers[id]?.let {
            database.databaseQueries.findAll().removeListener(it)
            observers.remove(id)
        }
    }

    fun findAll(): List<HeroEntity> = database
        .databaseQueries
        .findAll { id: Long, name: String, real_name: String, team: String, first_appearance: Long, created_by: String, publisher: String, avatar_url: String?, bio: String ->
            HeroEntity(
                id = id,
                name = name,
                realName = real_name,
                team = team,
                firstAppearance = first_appearance.toInt(),
                createdBy = created_by,
                publisher = publisher,
                avatarUrl = avatar_url,
                bio = bio
            )
        }.executeAsList()
}

internal var idCounter = atomic(0)