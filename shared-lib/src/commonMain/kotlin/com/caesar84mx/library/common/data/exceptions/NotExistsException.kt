package com.caesar84mx.library.common.data.exceptions

class NotExistsException(msg: String): Exception(msg)