package com.caesar84mx.library.common.kotlinextensions

import com.caesar84mx.library.common.data.repositories.backgroundDispatcher
import com.caesar84mx.library.common.data.repositories.mainDispatcher
import kotlinx.coroutines.*

fun launchInBackground(block: suspend CoroutineScope.() -> Unit) = CoroutineScope(backgroundDispatcher).launch { block() }

fun <T> asyncGetInBackground(block: suspend CoroutineScope.() -> T): Deferred<T> {
    return CoroutineScope(backgroundDispatcher).async { block() }
}

fun launchInMain(block: suspend CoroutineScope.() -> Unit) = CoroutineScope(mainDispatcher).launch { block() }
