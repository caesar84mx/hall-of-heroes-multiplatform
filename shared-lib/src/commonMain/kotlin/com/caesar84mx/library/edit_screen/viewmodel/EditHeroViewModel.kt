package com.caesar84mx.library.edit_screen.viewmodel

import dev.icerock.moko.mvvm.livedata.MutableLiveData
import dev.icerock.moko.mvvm.viewmodel.ViewModel

abstract class EditHeroViewModel : ViewModel() {
    val name: MutableLiveData<String> = MutableLiveData("")
    val realName: MutableLiveData<String> = MutableLiveData("")
    val team: MutableLiveData<String> = MutableLiveData("")
    val firstAppearance: MutableLiveData<String> = MutableLiveData("")
    val createdBy: MutableLiveData<String> = MutableLiveData("")
    val publisher: MutableLiveData<String> = MutableLiveData("")
    val avatarUrl: MutableLiveData<String> = MutableLiveData("")
    val bio: MutableLiveData<String> = MutableLiveData("")

    val status: MutableLiveData<Status> = MutableLiveData(Status.Idle)

    sealed class Status {
        object Idle: Status()
        object SavingSuccess: Status()
        object SavingError: Status()
    }

    abstract fun onSavePressed()
}