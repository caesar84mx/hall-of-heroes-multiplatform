package com.caesar84mx.library.edit_screen.viewmodel

import com.caesar84mx.library.common.data.model.HeroEntity
import com.caesar84mx.library.common.data.repositories.HeroesRepo
import com.caesar84mx.library.common.kotlinextensions.notNull

class EditHeroCommonViewModel(val id: Long = 0) : EditHeroViewModel() {
    private val repo = HeroesRepo()
    private var currentHero = HeroEntity()

    init {
        if (id != 0L) {
            currentHero = repo.get(id) ?: HeroEntity()
            currentHero.let {
                name.postValue(it.name)
                realName.postValue(it.realName)
                team.postValue(it.team)
                firstAppearance.postValue(it.firstAppearance.toString())
                createdBy.postValue(it.createdBy)
                publisher.postValue(it.publisher)
                avatarUrl.postValue(it.avatarUrl.notNull())
                bio.postValue(it.bio)
            }
        }
    }

    override fun onSavePressed() {
        readValues()

        try {
            if (currentHero.id == null) {
                repo.save(currentHero)
            } else {
                repo.update(currentHero)
            }
            status.postValue(Status.SavingSuccess)
        } catch (ex: Throwable) {
            println(ex.message)
            status.postValue(Status.SavingError)
        }
    }

    private fun readValues() {
        currentHero.let {
            it.name = name.value
            it.realName = realName.value
            it.team = team.value
            it.createdBy = createdBy.value
            it.publisher = publisher.value
            it.firstAppearance = firstAppearance.value.toInt()
            it.avatarUrl = avatarUrl.value
            it.bio = bio.value
        }
    }

    private fun resetToDefaults() {
        name.value = ""
        realName.value = ""
        team.value = ""
        firstAppearance.value = ""
        createdBy.value = ""
        publisher.value = ""
        avatarUrl.value = ""
        bio.value = ""
        status.value = Status.Idle
    }

    override fun onCleared() {
        resetToDefaults()
        super.onCleared()
    }
}