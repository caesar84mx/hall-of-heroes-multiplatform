package com.caesar84mx.library.common.data.repositories

import com.caesar84mx.library.Database
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.drivers.ios.NativeSqliteDriver
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import platform.darwin.dispatch_async
import platform.darwin.dispatch_get_main_queue
import platform.darwin.dispatch_queue_t
import kotlin.coroutines.CoroutineContext

internal actual fun getDriver(): SqlDriver? = NativeSqliteDriver(Database.Schema, "main.db")

internal actual val mainDispatcher: CoroutineDispatcher = NsQueueDispatcher(dispatch_get_main_queue())

internal actual val backgroundDispatcher: CoroutineDispatcher = mainDispatcher

internal class NsQueueDispatcher(
    private val dispatchQueue: dispatch_queue_t
): CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        dispatch_async(dispatchQueue) {
            block.run()
        }
    }
}